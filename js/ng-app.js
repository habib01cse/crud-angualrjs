
var myApp = angular.module("myApp",[]);
myApp.controller("myController", function($scope){

    $scope.newUser = {};
    $scope.clickedUser = {};
    $scope.message = "";

    $scope.users=[
        {username: "Feroz", fullName: "Md. Feroz", email: "feroz@gmail.com"},
        {username: "Hridoy", fullName: "Md. Hridoy", email: "hridoy@gmail.com"},
        {username: "Yeasin", fullName: "Md. Yeasin Alli", email: "yeasin@gmail.com"},
        {username: "Salim", fullName: "Salim Hossain", email: "salim@gmail.com"}

    ];

    $scope.saveUser = function(){
        $scope.users.push($scope.newUser);
        $scope.newUser = {};
        $scope.message = "New user added successfully!";
    }

    $scope.selectUser = function(user){
        console.log(user);
        $scope.clickedUser = user;
    }

    $scope.userUpdate = function(){
        $scope.message = "User updated successfully!";
    }
    $scope.deleteUser = function(){
        $scope.users.splice($scope.users.indexOf($scope.clickedUser),1);
        $scope.message = "User Delete successfully!";
    }

    $scope.clearMessage = function(){
        $scope.message = "";
    }

});


